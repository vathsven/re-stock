import './App.less';
import Company from './components/company';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import PriceWidget from './components/price-widget';
import { fetchCompanies, addCompany} from './redux/actions';
import { setSelectedCompany, removeCompany, refreshPricesRealtime } from './redux/actions';

const mapStateToProps = state => state.companyActionManager;
const mapDispatchToProps = dispatch => ({
  fetchCompanies: () => dispatch(fetchCompanies()),
  addCompany: company => dispatch(addCompany(company)),
  removeCompany: company => dispatch(removeCompany(company)),
  setSelectedCompany: company => dispatch(setSelectedCompany(company)),
  refreshPricesRealtime: prices => dispatch(refreshPricesRealtime(prices))
});

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      selectedCompany: props.selectedCompany,
      companiesList: props.companiesList,
      storedCompanies: props.storedCompanies,
      pricesRealtime: props.pricesRealtime
    };
    this.handleSelectChange = this.handleSelectChange.bind(this);
    this.handleAddCompany = this.handleAddCompany.bind(this);
    this.handleRemoveCompany = this.handleRemoveCompany.bind(this);
    this.handleRefreshPrices = this.handleRefreshPrices.bind(this);
  }

  handleSelectChange(e) {
    this.setState({selectedCompany: e.target.value});
    this.props.setSelectedCompany(e.target.value);
  }

  componentDidMount() {
    this.props.fetchCompanies();
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.storedCompanies.length > prevProps.storedCompanies.length)
      this.props.refreshPricesRealtime(this.props.storedCompanies);
  }

  handleAddCompany() {
    if (this.state.selectedCompany === '') return;
    this.props.addCompany(this.state.selectedCompany);
    this.props.setSelectedCompany(this.state.selectedCompany);
  }

  handleRemoveCompany(e) {
    this.props.removeCompany(e.target.id);
  }

  handleRefreshPrices() {
    this.props.refreshPricesRealtime(this.props.storedCompanies);
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">

          {this.props.companiesList && this.props.companiesList.length > 0 &&
            <Company {...this.props} change={this.handleSelectChange} 
              addButton={this.handleAddCompany} />
          }

          {this.props.storedCompanies && this.props.storedCompanies.length > 0 &&
            <PriceWidget {...this.props} refreshButton={this.handleRefreshPrices}
              removeButton={this.handleRemoveCompany} />
          }

        </header>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
