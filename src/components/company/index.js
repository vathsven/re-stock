import React from 'react';

const Company = (props) => 
    <>
      <select value={props.selectedCompany} onChange={props.change}>
        <option value=''>Select company ...</option>
        {props.companiesList.map((e, key) => {
          return <option key={key} value={e.ticker}>{e.ticker} / {e.name}</option>;
        })}}
      </select>

      <p className="less-test">
        <input type="button" value="Add" onClick={props.addButton} />
      </p>
    </>;

export default Company;