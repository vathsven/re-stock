import React from 'react';

const PriceWidget = (props) =>
  <>
    <ul>
    {props.storedCompanies.map((e) => {
      return <li key={e.ticker}>{e.name} /  
      <b className="stock-ticker">{e.ticker} {e.prices && <span className="last-price">${e.prices.last_price}</span>}
      {!e.prices && <span className="last-price">?</span>}
      </b>
      <button onClick={props.removeButton} id={e.ticker} 
      className="button-remove">X</button></li>;
    })}
    </ul>

    <p className="less-test">
      <input type="button" value="Refresh prices" onClick={props.refreshButton} />
    </p>
  </>;

export default PriceWidget;