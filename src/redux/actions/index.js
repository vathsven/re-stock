export const RECEIVE_COMPANIES = 'RECEIVE_COMPANIES';
export const RECEIVE_PRICES_REALTIME = 'RECEIVE_PRICES_REALTIME';
export const SET_COMPANY = 'SET_COMPANY';
export const ADD_COMPANY = 'ADD_COMPANY';
export const REMOVE_COMPANY = 'REMOVE_COMPANY';

const API_COMPANIES_URL = 'https://api-v2.intrinio.com/companies?api_key=OmQxYWFhN2I3ZjE0YjA3MzFhMzkxMmNiMTFlODU2NDdh';

export const receiveCompanies = companiesList => ({
  type: RECEIVE_COMPANIES,
  companiesList
});

export const addCompany = company => ({
  type: ADD_COMPANY,
  payload: company
});

export const removeCompany = company => ({
  type: REMOVE_COMPANY,
  payload: company
});

export const setSelectedCompany = company => ({
  type: SET_COMPANY,
  payload: company
});

export const receivePricesRealtime = price => ({
  type: RECEIVE_PRICES_REALTIME,
  price
});

export const fetchCompanies = () => {
  return function (dispatch) {
    return fetch(API_COMPANIES_URL)
      .then(response => response.json())
      .then(data => dispatch(receiveCompanies(data.companies)))
      .catch(err => console.log(err));
  }
};

export const fetchPrices = (ticker) => {
  return function (dispatch) {
    const API_PRICES_URL = `https://api-v2.intrinio.com/securities/${ticker}/prices/realtime?api_key=OmQxYWFhN2I3ZjE0YjA3MzFhMzkxMmNiMTFlODU2NDdh`
    return fetch(API_PRICES_URL)
      .then(response => response.json())
      .then(data => dispatch(receiveCompanies(data.companies)))
      .catch(err => console.log(err));
  }
};

export const refreshPricesRealtime = (prices) => {
  return function(dispatch) {
    var urls = prices.map(item => `https://api-v2.intrinio.com/securities/${item.ticker}/prices/realtime?api_key=OmQxYWFhN2I3ZjE0YjA3MzFhMzkxMmNiMTFlODU2NDdh`);
    return Promise.all(urls.map(url =>
        fetch(url)
          .then(response => response.json())
          .then(data => dispatch(receivePricesRealtime(data)))
    ));
  }
}