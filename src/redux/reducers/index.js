import { combineReducers } from 'redux';
import { RECEIVE_COMPANIES, ADD_COMPANY } from '../actions';
import { REMOVE_COMPANY, SET_COMPANY, RECEIVE_PRICES_REALTIME} from '../actions';

var initState = {
  selectedCompany: '',
  companiesList: [],
  storedCompanies: [],
  pricesRealtime: [],
};

// Local state snapshot ?
const localState = localStorage.getItem('localState');
if (localState) {
  initState = JSON.parse(localState);
}

function storeState(state) {
  localStorage.setItem('localState', JSON.stringify(state));
  return state;
}

const companyActionManager = (state = initState, action) => {

  var _storedCompanies = [];
  var _pricesRealtime = [];

  switch (action.type) {

    case RECEIVE_PRICES_REALTIME:
      // Exclude old price
      _pricesRealtime = state.pricesRealtime
        .filter(item => item.security.ticker !== action.price.security.ticker);
      _pricesRealtime.push(action.price);
      // Update companies -> add price 
      _storedCompanies = state.storedCompanies.map(item => {
        let price_obj = _pricesRealtime.filter(price => price.security.ticker === item.ticker);
        return {...item, prices: price_obj[0]};
      });
      return storeState({...state, pricesRealtime: _pricesRealtime, storedCompanies: _storedCompanies});

    case RECEIVE_COMPANIES:
      return storeState({...state, companiesList: action.companiesList});

    case ADD_COMPANY:
      var double = state.storedCompanies.filter(item => item.ticker === action.payload);
      if (double[0]) return state;
      var company = state.companiesList.filter(item => item.ticker === action.payload);
      return storeState({...state, storedCompanies: [...state.storedCompanies, company[0]]})

    case REMOVE_COMPANY:
      _storedCompanies = state.storedCompanies.filter(item => item.ticker !== action.payload);
      return storeState({...state, storedCompanies: _storedCompanies});

    case SET_COMPANY:
      return storeState({...state, selectedCompany: action.payload});

    default:
      return state;
  }
}

const rootReducer = combineReducers({companyActionManager});

export default rootReducer;